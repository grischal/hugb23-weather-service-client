import grpc

# Imports the grpc helper files for the weather service
import grpc_helpers.weatherStation_pb2 as weatherStation_pb2
import grpc_helpers.weatherStation_pb2_grpc as weatherStation_pb2_grpc

def run():
   #Replace this with the api key you received via email
   API_KEY = 'enter api key'
   SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
   try:
      #Here is a difference to the client we have used so far: We are using a secure channel.
      with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:

         #This is just as before: We import the grpc definitions.
         stub = weatherStation_pb2_grpc.WeatherStationStub(channel)

         #Call the three different methods. For parameter and return type descritions, see the proto file
         current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=64.21, lon=-21.19, api_key=API_KEY))
         forecast = stub.get_four_day_forecast(weatherStation_pb2.Location(lat=64.21, lon=-21.19, api_key=API_KEY))
         historical = stub.get_historical_forecast(weatherStation_pb2.HistoryPlace(location_descriptor="Reykjavik,IS", start_time=1665580000, end_time=1665618000, api_key=API_KEY))

   #Error handling in case the grpc connection throws an error. This is how the service provides errors.
   except grpc.RpcError as e:
      # print gRPC error message
      print(e.details())
      status_code = e.code()
      # should print `INVALID_ARGUMENT`
      print(status_code.name)
      # should print `(3, 'invalid argument')`
      print(status_code.value)
   else:
      #If no error, print the return values of the three different calls.
      print("Current weather: " , current_weather)
      print("4-day forecast: " , forecast.forecast)
      print("Historical forecast: " , historical.forecast)

run()